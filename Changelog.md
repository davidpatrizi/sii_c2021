##Changelog

## Versión [3.0] 02/12/2020

## Añadido
- logger.cpp, ejecutable para comunicarse a través de la tubería FIFOtenis, mensajes del marcador del partido.
- bot.cpp, ejecutable que se encarga del movimiento de la raqueta del jugador 1, a través de archivo proyectado en memoria compartida. 
- DatosMemCompartida.h para comunicación entre bot.cpp y Mundo.

## Modificado
- CMakeLists.txt para generar ejecutables bot y logger.
- Mundo.cpp y Mundo.h para implementar clases bot y logger.
- Finalización de juego cuando uno de los dos jugadores llega a 3 puntos en su marcador.


## Versión [2.0] 18/11/2020

## Añadido
 
Changelog.md
 
## Modificado

Esfera.cpp y Raqueta.cpp para disminuir el tamaño de la pelota y añadir movimiento a las raquetas.

## Versión [1.1] 25/10/2020
Primer commit Changelog


